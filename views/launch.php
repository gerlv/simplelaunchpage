      <div class="row">
        <div class="span8">
          <div>
            <!-- start slipsum code -->
            <h2>Special Composite Rebar</h2>
            <p>Currently, our website is still in development. If you want to hear when our website is going live, please enter your email in the form on the right. We will let you know the same hour it is live.</p>
            <h3>Privacy policy</h3>
            <p>At this moment the only reason why we get email addresses is to notify subscribers when our website is live. We will not sell or distribute your email address.</p>
            <p>Additionally, we will not send any more emails after we notify you that website is live.
            <p>We will have separate newsletter signup form on our website once it is live.</p>
            <!-- end slipsum code -->
          </div>
        </div>
        <div class="span1">&nbsp;</div>
        <div class="span7">
          <h1>Add your email</h1>
          <p>You will be notified when the service is live</p>
          <?php echo validation_errors(); ?>
          <form action="/" method="post">
            <div class="clearfix">
              <input type="text" class="email" value="<?php echo set_value('email'); ?>" name="email" id="email" />
            </div>
            <div>
              <input type="submit" name="submit" class="btn primary" id="submit" value="Submit" />
            </div>
          </form>
        </div>
      </div>
