<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Launch extends CI_Controller {
	public function index()	{
    $this->load->helper(array('form','url'));
    $this->load->library('form_validation');
    $this->load->model('launchmodel');

    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
  
    if( $this->form_validation->run() == FALSE) {
      $this->load->view('header');
		  $this->load->view('launch');
      $this->load->view('footer');
    } 
    else {
      $this->launchmodel->save($this->input->post('email'));
      redirect('thanks');
    }
	}
}
