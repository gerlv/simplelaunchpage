<?php
class Launchmodel extends CI_Model {
  public function save($email) {
    $data = array(
      'email' => $email,
      'subscribed' => date('Y-m-d H:i:s')
    );

    $this->db->insert('launch', $data);
  }
}
