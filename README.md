Simple launch page

Add following .htaccess to your root:

Last login: Fri Oct  7 13:35:20 on ttys000

RewriteEngine On
RewriteBase /
RewriteCond %{REQUEST_URI} ^system.*
RewriteRule ^(.*)$ /index.php?/$1 [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php?/$1 [L]

And create table 'launch' with following fields:
email - varchar255
subscribed - datetime
